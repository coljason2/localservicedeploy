package com.Util;

import java.awt.Font;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Properties;
import javax.swing.JCheckBox;
import com.MainFrame.CommanFrame;
import com.MainFrame.textArea;

import lombok.Getter;
import lombok.Setter;

public class PropertiesUtil {

	private File configFile = new File("config.properties");
	private Properties props = new Properties();
	private FileReader reader = null;
	@Getter
	@Setter
	private textArea textCmd = null;
	@Getter
	@Setter
	private CommanFrame CommanFrame = null;
	@Setter
	private Font textFont = null;
	@Setter
	@Getter
	private JCheckBox onTopCheckBox = null;
	@Setter
	private JCheckBox isResizeWin = null;

	public void SaveConfig() {
		try {
			props.setProperty("font_name", textFont.getFontName());
			props.setProperty("font_size", String.valueOf(textFont.getSize()));
			props.setProperty("frame_height", String.valueOf(CommanFrame.getLocalService().getHeight()));
			props.setProperty("frame_width", String.valueOf(CommanFrame.getLocalService().getWidth()));
			props.setProperty("frame_x", String.valueOf(CommanFrame.getLocalService().getX()));
			props.setProperty("frame_y", String.valueOf(CommanFrame.getLocalService().getY()));
			props.setProperty("isontop_state", String.valueOf(onTopCheckBox.isSelected()));
			props.setProperty("resize_state", String.valueOf(isResizeWin.isSelected()));
			FileWriter writer = new FileWriter(configFile);
			props.store(writer, "Default UI settings");
			writer.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Font setInitialeTextFont() {
		try {
			reader = new FileReader(configFile);
			props.load(reader);
			String font_name = props.getProperty("font_name");
			int font_size = Integer.parseInt(props.getProperty("font_size"));
			reader.close();
			textFont = new Font(font_name, Font.PLAIN, font_size);
		} catch (Exception ex) {
			textFont = new Font("新細明體", Font.PLAIN, 12); // default;
		}
		return textFont;
	}

	public void setInitializeFrameProperty() {
		try {
			reader = new FileReader(configFile);
			props.load(reader);
			int frame_height = Integer.parseInt(props.getProperty("frame_height"));
			int frame_width = Integer.parseInt(props.getProperty("frame_width"));
			int frame_x = Integer.parseInt(props.getProperty("frame_x"));
			int frame_y = Integer.parseInt(props.getProperty("frame_y"));
			reader.close();
			CommanFrame.getLocalService().setBounds(frame_x, frame_y, frame_width, frame_height);
			if (getCheckBoxStatus("isontop_state"))
				CommanFrame.getLocalService().setAlwaysOnTop(true);
			else
				CommanFrame.getLocalService().setAlwaysOnTop(false);
			if (getCheckBoxStatus("resize_state"))
				CommanFrame.getLocalService().setResizable(true);
			else
				CommanFrame.getLocalService().setResizable(false);
		} catch (Exception ex) {
			CommanFrame.getLocalService().setBounds(100, 100, 682, 338); // default
		}
	}

	public boolean getCheckBoxStatus(String state) {
		Boolean isSelect = false;
		try {
			FileReader reader = new FileReader(configFile);
			props.load(reader);
			String checkbox_state = props.getProperty(state);
			reader.close();
			isSelect = checkbox_state.equals("true");
		} catch (Exception ex) {
			return isSelect;
		}
		return isSelect;
	}

}
