package com.Util;

public interface URLContent {

	public static interface LobbyURL {
		public static final String Keno = "http://lobby-keno-local-dev.sgplay.biz/html5";
		public static final String KenoSSL = "https://lobby-keno-local-dev.sgplay.biz/html5";
		public static final String ilotto = "http://lobby-ilotto-local-dev.sgplay.biz/html5";
		public static final String ilottoSSL = "https://lobby-ilotto-local-dev.sgplay.biz/html5";
		public static final String SG = "http://lobby-local-dev.sgplay.biz/list.html";
		public static final String SGSSL = "https://lobby-local-dev.sgplay.biz/list.html";
	}

	public static interface BackOfficeURL {
		public static final String KenoIP = "http://127.0.0.1:8806";
		public static final String Keno = "http://bo-keno-local-dev.sgplay.biz";
		public static final String KenoSSL = "https://bo-keno-local-dev.sgplay.biz";
		public static final String ilottoIP = "http://127.0.0.1:8806";
		public static final String ilotto = "http://bo-ilotto-local-dev.sgplay.biz";
		public static final String ilottoSSL = "https://bo-ilotto-local-dev.sgplay.biz";
		public static final String SGIP = "http://127.0.0.1:8806";
		public static final String SG = "http://bo-local-dev.sgplay.biz";
		public static final String SGSSL = "https://bo-local-dev.sgplay.biz";
	}

	public static interface ApiURL {
		public static final String KenoIP = "http://127.0.0.1:18804/apiportal";
		public static final String Keno = "http://api-keno-local-dev.sgplay.biz";
		public static final String KenoSSL = "https://api-keno-local-dev.sgplay.biz";
		public static final String ilottoIP = "http://127.0.0.1:8806/apiportal";
		public static final String ilotto = "http://api-ilotto-local-dev.sgplay.biz";
		public static final String ilottoSSL = "https://api-ilotto-local-dev.sgplay.biz";
	}

}
