package com.MainFrame;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class PathSetting {
	private ServerSocket srvSocket = null;
	private final File now_directory = new File(".");
	private final String current_path = now_directory.getAbsolutePath().replaceAll("\\.", "");
	private File Build_bat = new File(current_path + "/conf/Build.bat");
	private File DeployToLocalDev_bat = new File(current_path + "/conf/DeployToLocalDev.bat");
	private File GameServerBuild_bat = new File(current_path + "/conf/GameServerBuild.bat");
	private File ApiBuild_bat = new File(current_path + "/conf/ApiBuild.bat");
	private File BackofficeBuild_bat = new File(current_path + "/conf/BackofficeBuild.bat");
	private File sites_dev_conf = new File(current_path + "/nginx/conf/sites/sites-dev.conf");
	private File sites_lobby_ssl_conf = new File(current_path + "/nginx/conf/sites/sites-lobby-ssl.conf");

	private String project_path;
	private String local_dev_path;
	private String sites_dev_path;
	private String sites_lobby_ssl_path;
	private JFrame frmPathsetting;

	private JTextField project_path_txt;
	private JTextField local_dev_path_txt;
	private JLabel lblNewLabel_1;
	private JTextField sites_dev_path_txt;
	private JTextField sites_lobby_ssl_path_txt;

	/**
	 * Create the application.
	 */
	public PathSetting() {
		if (checkSingleInstance(45454)) {
			readbatfile();
			initialize();
		}
	}

	private void readbatfile() {
		project_path = getVariable(Build_bat, "@set eclipse_path=");
		local_dev_path = getVariable(DeployToLocalDev_bat, "@set local_dev_path=");
		sites_dev_path = getVariable(sites_dev_conf, "root ");
		sites_lobby_ssl_path = getVariable(sites_lobby_ssl_conf, "root ");
	}

	private String getVariable(File file, String variableName) {
		InputStream is = null;
		String getString = null;
		try {
			is = new FileInputStream(file);
			String output = IOUtils.toString(is, "BIG5");
			String split = output.substring(output.indexOf(variableName));
			if (split.indexOf("=") == -1) {
				getString = split.substring(split.indexOf(variableName) + 5, split.indexOf(";"));
			} else {
				getString = split.substring(split.indexOf("=") + 1, split.indexOf("\n")).replace("\"", "");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);
		}
		return getString.trim();
	}

	private String getContent(File file) {
		InputStream is = null;
		String output = null;
		try {
			is = new FileInputStream(file);
			output = IOUtils.toString(is, "BIG5");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);
		}
		return output;
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		frmPathsetting = new JFrame();
		frmPathsetting.setResizable(false);
		frmPathsetting.setTitle("Path Setting");
		frmPathsetting.setBounds(100, 100, 511, 186);
		frmPathsetting.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmPathsetting.getContentPane().setLayout(null);
		frmPathsetting.setLocationRelativeTo(null);
		JLabel lblNewLabel = new JLabel("Source project path :");
		lblNewLabel.setBounds(10, 10, 123, 15);
		frmPathsetting.getContentPane().add(lblNewLabel);

		project_path_txt = new JTextField();
		project_path_txt.setText(project_path);
		project_path_txt.setBounds(172, 7, 313, 21);
		frmPathsetting.getContentPane().add(project_path_txt);
		project_path_txt.setColumns(10);

		local_dev_path_txt = new JTextField();
		local_dev_path_txt.setText(local_dev_path);
		local_dev_path_txt.setBounds(172, 35, 314, 21);
		frmPathsetting.getContentPane().add(local_dev_path_txt);
		local_dev_path_txt.setColumns(10);

		lblNewLabel_1 = new JLabel("Local dev path :");
		lblNewLabel_1.setBounds(10, 38, 111, 15);
		frmPathsetting.getContentPane().add(lblNewLabel_1);

		JButton btnNewButton = new JButton("Confirm");

		btnNewButton.setBounds(172, 121, 87, 23);
		frmPathsetting.getContentPane().add(btnNewButton);

		JLabel lblSitesdevpath = new JLabel("sites_dev_path:");
		lblSitesdevpath.setBounds(10, 68, 111, 15);
		frmPathsetting.getContentPane().add(lblSitesdevpath);

		JLabel lblSiteslobbysslpath = new JLabel("sites_lobby_path:");
		lblSiteslobbysslpath.setBounds(10, 93, 151, 15);
		frmPathsetting.getContentPane().add(lblSiteslobbysslpath);

		sites_dev_path_txt = new JTextField();
		sites_dev_path_txt.setText((String) null);
		sites_dev_path_txt.setColumns(10);
		sites_dev_path_txt.setBounds(171, 63, 314, 21);
		sites_dev_path_txt.setText(sites_dev_path);
		frmPathsetting.getContentPane().add(sites_dev_path_txt);

		sites_lobby_ssl_path_txt = new JTextField();
		sites_lobby_ssl_path_txt.setText((String) null);
		sites_lobby_ssl_path_txt.setColumns(10);
		sites_lobby_ssl_path_txt.setBounds(171, 90, 314, 21);
		sites_lobby_ssl_path_txt.setText(sites_lobby_ssl_path);
		frmPathsetting.getContentPane().add(sites_lobby_ssl_path_txt);
		frmPathsetting.setVisible(true);

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String build = getContent(Build_bat);
				String gsbuild = getContent(GameServerBuild_bat);
				String apibuild = getContent(ApiBuild_bat);
				String bobuild = getContent(BackofficeBuild_bat);
				String deploy = getContent(DeployToLocalDev_bat);
				String sites_dev = getContent(sites_dev_conf);
				String sites_lobby = getContent(sites_lobby_ssl_conf);

				String replacebuild = build.replace(project_path, project_path_txt.getText());
				String replace_gsbuild = gsbuild.replace(project_path, project_path_txt.getText());
				String replace_apibuild = apibuild.replace(project_path, project_path_txt.getText());
				String replace_bobuild = bobuild.replace(project_path, project_path_txt.getText());
				String replacedeploy = deploy.replace(local_dev_path, local_dev_path_txt.getText())
						.replace(project_path, project_path_txt.getText());
				String replace_sites_dev = sites_dev.replace(sites_dev_path, sites_dev_path_txt.getText());
				String replace_sites_lobby_ssl = sites_lobby.replace(sites_lobby_ssl_path,
						sites_lobby_ssl_path_txt.getText());

				try {
					FileUtils.writeStringToFile(Build_bat, replacebuild, "BIG5");
					FileUtils.writeStringToFile(GameServerBuild_bat, replace_gsbuild, "BIG5");
					FileUtils.writeStringToFile(ApiBuild_bat, replace_apibuild, "BIG5");
					FileUtils.writeStringToFile(BackofficeBuild_bat, replace_bobuild, "BIG5");
					FileUtils.writeStringToFile(DeployToLocalDev_bat, replacedeploy, "BIG5");
					FileUtils.writeStringToFile(sites_dev_conf, replace_sites_dev, "BIG5");
					FileUtils.writeStringToFile(sites_lobby_ssl_conf, replace_sites_lobby_ssl, "BIG5");

					JOptionPane.showMessageDialog(null, "Edit Success", "Message", JOptionPane.INFORMATION_MESSAGE,
							null);
					srvSocket.close();
					frmPathsetting.dispose();
				} catch (Exception f) {
					f.printStackTrace();
				}

			}
		});
		frmPathsetting.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					srvSocket.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

	public boolean checkSingleInstance(int srvPort) {
		boolean isOpen = true;
		try { // 啟用ServerSocket，用以控制只能開啟一個程序
			srvSocket = new ServerSocket(srvPort);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex.getMessage().indexOf("Address already in use: JVM_Bind") >= 0) {
				isOpen = false;
			} else {
				isOpen = true;
			}
		}
		return isOpen;
	}

	public JFrame getFrmPathsetting() {
		return frmPathsetting;
	}
}
