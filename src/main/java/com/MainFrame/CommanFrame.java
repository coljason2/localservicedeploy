package com.MainFrame;

import java.awt.BorderLayout;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.Button.ButtonAPIBuild;
import com.Button.ButtonBoBuild;
import com.Button.ButtonBuild;
import com.Button.ButtonDeploy;
import com.Button.ButtonGSBuild;
import com.Button.ButtonNginx;
import com.Button.ButtonStart;
import com.Button.ButtonStartBackoffice;
import com.Button.ButtonStartGameServer;
import com.Button.ButtonStartLobby;
import com.Button.ButtonStop;
import com.MenuItem.FontMenuItem;
import com.MenuItem.HelpMenuItem;
import com.MenuItem.LinkMenuItem;
import com.MenuItem.MenuScroller;
import com.MenuItem.PathMenuItem;
import com.MenuItem.SizeMenuItem;
import com.Util.PropertiesUtil;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JToolBar;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JCheckBox;

public class CommanFrame implements ItemListener {
	private JFrame LocalService;
	private static textArea textCmd = new textArea();
	protected static PropertiesUtil PropertiesUtil = null;
	@SuppressWarnings("unused")
	private static ServerSocket srvSocket = null;
	// Button
	private final JButton btnStart = new ButtonStart();
	private final JButton btnStop = new ButtonStop();
	private final JButton btnBuild = new ButtonBuild();
	private final JButton btnDeploy = new ButtonDeploy();
	private final JButton btnGs = new ButtonStartGameServer();
	private final JButton btnBo = new ButtonStartBackoffice();
	private final JButton btnGame = new ButtonStartLobby();
	private final JButton btnNginx = new ButtonNginx();
	private final JButton btnGsBuild = new ButtonGSBuild();
	private final JButton btnApiBuild = new ButtonAPIBuild();
	private final JButton btnBoBuild = new ButtonBoBuild();
	// Menu
	private JMenu mnNewMenu = new JMenu("WebLink");
	private JMenu mnSetting = new JMenu("Setting");
	private JMenu mnWindows = new JMenu("Windows");
	private JMenu mnHelp = new JMenu("Help");
	private PathMenuItem mmtmPathSetting = new PathMenuItem("Path Setting");
	private JMenu mmtmFont = new JMenu("Font Setting");
	private JMenu mmtmFontSize = new JMenu("Font Size");
	private HelpMenuItem mmtmAbout = new HelpMenuItem("About");
	private JMenuBar menuBar = new JMenuBar();
	private MenuScroller fontScrollPane = new MenuScroller(mmtmFont, 20);
	private List<SizeMenuItem> sizeMenuItems = new ArrayList<SizeMenuItem>();
	private List<FontMenuItem> fontMenuItems = new ArrayList<FontMenuItem>();
	private final JToolBar toolBar = new JToolBar();
	private final JCheckBox onTopCheckBox = new JCheckBox("AlwaysOnTop");
	private final JCheckBox isResizeWin = new JCheckBox("isResizeWindows");
	private final JToolBar toolBarGsBoLob = new JToolBar();
	private final JToolBar toolBarStartStop = new JToolBar();
	private final JToolBar toolBarBuild = new JToolBar();


	/**
	 * @param urllist
	 * @wbp.parser.entryPoint
	 */
	public void initialize(String title, Map<String, String> urllist) {
		initializePropertiesUtil();
		setLocalService(new JFrame());
		PropertiesUtil.setInitializeFrameProperty();
		initialFontSizeItems();
		initializeMenu();
		initializeToolBar();
		initialLinkMenu(urllist);
		getLocalService().setJMenuBar(menuBar);
		getLocalService().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getLocalService().setTitle(title);
		JPanel panel = (JPanel) getLocalService().getContentPane();
		panel.add(textCmd, BorderLayout.CENTER);
		LocalService.getContentPane().add(toolBar, BorderLayout.NORTH);
		toolBar.add(toolBarStartStop);
		toolBar.add(toolBarBuild);
		toolBar.add(toolBarGsBoLob);
		onTopCheckBox.setSelected(PropertiesUtil.getCheckBoxStatus("isontop_state"));
		isResizeWin.setSelected(PropertiesUtil.getCheckBoxStatus("resize_state"));

	}

	private void initializeToolBar() {
		toolBarStartStop.add(btnStart);
		toolBarStartStop.add(btnStop);
		toolBarBuild.add(btnBuild);
		toolBarBuild.add(btnGsBuild);
		toolBarBuild.add(btnApiBuild);
		toolBarBuild.add(btnBoBuild);
		toolBarBuild.add(btnDeploy);
		toolBarGsBoLob.add(btnGs);
		toolBarGsBoLob.add(btnBo);
		toolBarGsBoLob.add(btnGame);
		toolBarGsBoLob.add(btnNginx);
	}

	private void initializePropertiesUtil() {
		PropertiesUtil = new PropertiesUtil();
		PropertiesUtil.setCommanFrame(this);
		PropertiesUtil.setTextCmd(textCmd);
		PropertiesUtil.setInitialeTextFont();
		PropertiesUtil.setOnTopCheckBox(onTopCheckBox);
		PropertiesUtil.setIsResizeWin(isResizeWin);
	}

	@SuppressWarnings("static-access")
	private void initialFontSizeItems() {
		for (int i = 10; i <= 24; i++) {
			SizeMenuItem sizeMenuItem = new SizeMenuItem(String.valueOf(i));
			sizeMenuItem.setPropertiesUtil(PropertiesUtil);
			sizeMenuItems.add(sizeMenuItem);
		}
		GraphicsEnvironment graphicsEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] listOfFontFamilyNames = graphicsEnv.getAvailableFontFamilyNames();
		for (String f : listOfFontFamilyNames) {
			FontMenuItem fontMenuItem = new FontMenuItem(f);
			fontMenuItem.setPropertiesUtil(PropertiesUtil);
			fontMenuItems.add(fontMenuItem);
		}
		fontScrollPane.setBottomFixedCount(9);
	}

	private void initialLinkMenu(Map<String, String> urllist) {
		JMenu weblink = new JMenu("Link");
		JMenu SSLlink = new JMenu("SSLLink");
		weblink.add(new LinkMenuItem("LobbyWeb").setURL(urllist.get("LobbyWeb")));
		weblink.add(new LinkMenuItem("MerchantWeb").setURL(urllist.get("MerchantWeb")));
		weblink.add(new LinkMenuItem("ApiWeb").setURL(urllist.get("ApiWeb")));
		weblink.add(new LinkMenuItem("ApiIP").setURL(urllist.get("ApiIP")));
		weblink.add(new LinkMenuItem("BoIP").setURL(urllist.get("BoIP")));
		SSLlink.add(new LinkMenuItem("LobbyWebSSL").setURL(urllist.get("LobbyWebSSL")));
		SSLlink.add(new LinkMenuItem("MerchantWebSSL").setURL(urllist.get("MerchantWebSSL")));
		SSLlink.add(new LinkMenuItem("ApiWebSSL").setURL(urllist.get("ApiWebSSL")));
		mnNewMenu.add(weblink);
		mnNewMenu.add(SSLlink);
	}

	private void initializeMenu() {

		for (FontMenuItem f : fontMenuItems) {
			mmtmFont.add(f);
		}
		for (SizeMenuItem s : sizeMenuItems) {
			mmtmFontSize.add(s);
		}
		mnSetting.add(mmtmPathSetting);
		mnSetting.add(mmtmFont);
		mnSetting.add(mmtmFontSize);
		menuBar.add(mnNewMenu);
		menuBar.add(mnSetting);
		menuBar.add(mnWindows);
		menuBar.add(mnHelp);
		mnHelp.add(mmtmAbout);
		mnWindows.add(onTopCheckBox);
		mnWindows.add(isResizeWin);
	}

	protected void checkSingleInstance(int srvPort) {
		try { // 啟用ServerSocket，用以控制只能開啟一個程序
			srvSocket = new ServerSocket(srvPort);
		} catch (Exception ex) {
			if (ex.getMessage().indexOf("Address already in use: JVM_Bind") >= 0) {
				JOptionPane.showMessageDialog(getLocalService(), "該程式已啟動", "提示", JOptionPane.OK_OPTION);
				System.exit(0);
			}
		}
	}

	public JFrame getLocalService() {
		return LocalService;
	}

	public void setLocalService(JFrame localService) {
		LocalService = localService;
		LocalService.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				PropertiesUtil.SaveConfig();
			}
		});
	}

	public static textArea getTextCmd() {
		return textCmd;
	}

	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == onTopCheckBox) {
			if (e.getStateChange() == ItemEvent.SELECTED)
				getLocalService().setAlwaysOnTop(true);
			else
				getLocalService().setAlwaysOnTop(false);
		} else if (e.getSource() == isResizeWin) {
			if (e.getStateChange() == ItemEvent.SELECTED)
				getLocalService().setResizable(true);
			else
				getLocalService().setResizable(false);
		}

	}

}
