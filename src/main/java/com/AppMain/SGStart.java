package com.AppMain;

import java.awt.EventQueue;
import java.util.Map;

import com.MainFrame.CommanFrame;
import com.Util.URLContent;
import com.google.common.collect.ImmutableMap;

public class SGStart extends CommanFrame {

	private static final String title = "SGLocalService";
	public static final Map<String, String> UrlList = new ImmutableMap.Builder<String, String>()
			.put("LobbyWeb", URLContent.LobbyURL.SG).put("LobbyWebSSL", URLContent.LobbyURL.SGSSL)
			.put("MerchantWeb", URLContent.BackOfficeURL.SG).put("MerchantWebSSL", URLContent.BackOfficeURL.SGSSL)
			// .put("ApiWeb", URLContent.ApiURL.ilotto)
			// .put("ApiWebSSL", URLContent.ApiURL.ilottoSSL)
			// .put("ApiIP", URLContent.ApiURL.ilottoIP)
			.put("BoIP", URLContent.BackOfficeURL.SGIP).build();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					SGStart window = new SGStart();
					window.checkSingleInstance(12458);
					window.getLocalService().setVisible(true);
				} catch (Exception e) {
					getTextCmd().getTextA().append(e.getMessage());
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SGStart() {
		initialize(title, UrlList);
	}

}
