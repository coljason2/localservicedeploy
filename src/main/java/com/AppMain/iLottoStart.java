package com.AppMain;

import java.awt.EventQueue;
import java.util.Map;

import com.MainFrame.CommanFrame;
import com.Util.URLContent;
import com.google.common.collect.ImmutableMap;

public class iLottoStart extends CommanFrame {

	private static final String title = "iLottoLocalService";
	public static final Map<String, String> UrlList = new ImmutableMap.Builder<String, String>()
			.put("LobbyWeb", URLContent.LobbyURL.ilotto).put("LobbyWebSSL", URLContent.LobbyURL.ilottoSSL)
			.put("MerchantWeb", URLContent.BackOfficeURL.ilotto)
			.put("MerchantWebSSL", URLContent.BackOfficeURL.ilottoSSL).put("ApiWeb", URLContent.ApiURL.ilotto)
			.put("ApiWebSSL", URLContent.ApiURL.ilottoSSL).put("ApiIP", URLContent.ApiURL.ilottoIP)
			.put("BoIP", URLContent.BackOfficeURL.ilottoIP).build();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					iLottoStart window = new iLottoStart();
					window.checkSingleInstance(12346);
					window.getLocalService().setVisible(true);
				} catch (Exception e) {
					getTextCmd().getTextA().append(e.getMessage());
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public iLottoStart() {
		initialize(title, UrlList);
	}

}
