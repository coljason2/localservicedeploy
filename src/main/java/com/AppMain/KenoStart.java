package com.AppMain;

import java.awt.EventQueue;
import java.util.Map;

import com.MainFrame.CommanFrame;
import com.google.common.collect.ImmutableMap;
import com.Util.URLContent;

public class KenoStart extends CommanFrame {

	private static final String title = "KenoService";
	public static final Map<String, String> UrlList = new ImmutableMap.Builder<String, String>()
			.put("LobbyWeb", URLContent.LobbyURL.Keno).put("LobbyWebSSL", URLContent.LobbyURL.KenoSSL)
			.put("MerchantWeb", URLContent.BackOfficeURL.Keno).put("MerchantWebSSL", URLContent.BackOfficeURL.KenoSSL)
			.put("ApiWeb", URLContent.ApiURL.Keno).put("ApiWebSSL", URLContent.ApiURL.KenoSSL)
			.put("ApiIP", URLContent.ApiURL.KenoIP).put("BoIP", URLContent.BackOfficeURL.KenoIP).build();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {

					KenoStart window = new KenoStart();
					window.checkSingleInstance(12345);
					window.getLocalService().setVisible(true);

				} catch (Exception e) {
					getTextCmd().getTextA().append(e.getMessage());
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public KenoStart() {
		initialize(title, UrlList);
	}

}
