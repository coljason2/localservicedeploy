package com.Button;

import java.awt.event.ActionEvent;
import com.CommandBat.StartBoServer;

public class ButtonStartBackoffice extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonStartBackoffice() {
		this.setText("BackOffice");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new StartBoServer(textCmd.getTextA()));
	}

}
