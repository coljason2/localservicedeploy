package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.StopBat;

public class ButtonStop extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonStop() {
		this.setText("Stop");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new StopBat(textCmd.getTextA()));
	}

}
