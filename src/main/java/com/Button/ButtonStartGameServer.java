package com.Button;

import java.awt.event.ActionEvent;
import com.CommandBat.StartGameServer;

public class ButtonStartGameServer extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonStartGameServer() {
		this.setText("GameServer");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute( new StartGameServer(textCmd.getTextA()));
	}

}
