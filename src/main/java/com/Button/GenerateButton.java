package com.Button;

import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JButton;

import com.MainFrame.textArea;

import redis.clients.jedis.Jedis;

public abstract class GenerateButton extends JButton implements ActionListener {
	private static final long serialVersionUID = 1L;
	protected static textArea textCmd;
	private Jedis jedis = new Jedis("localhost", 9379);
	protected final ExecutorService singleThreadExecutor = Executors.newCachedThreadPool();

	public GenerateButton() {
		addActionListener(this);
	}

	public void cleanRedis() {
		jedis.del("com.ct.lk.site.app.action.merchant.LeftMenuAction");
		jedis.disconnect();
	}
}
