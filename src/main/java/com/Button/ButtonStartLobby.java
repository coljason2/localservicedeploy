package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.StartLobbyBat;

public class ButtonStartLobby extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonStartLobby() {
		this.setText("Lobby");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new StartLobbyBat(textCmd.getTextA()));
	}

}
