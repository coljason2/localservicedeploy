package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.BackofficeBuildBat;

public class ButtonBoBuild extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonBoBuild() {
		this.setText("BoBuild");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new BackofficeBuildBat(textCmd.getTextA()));
	}

}
