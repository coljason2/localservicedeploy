package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.DeployBat;

public class ButtonDeploy extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonDeploy() {
		this.setText("Deploy");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new DeployBat(textCmd.getTextA()));
	}

}
