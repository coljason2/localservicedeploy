package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.BuildBat;

public class ButtonBuild extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonBuild() {
		this.setText("Build");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new BuildBat(textCmd.getTextA()));
	}

}
