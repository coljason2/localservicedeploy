package com.Button;

import java.awt.event.ActionEvent;
import com.CommandBat.StartNginxBat;

public class ButtonNginx extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonNginx() {
		this.setText("Nginx");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new StartNginxBat(textCmd.getTextA()));
	}

}
