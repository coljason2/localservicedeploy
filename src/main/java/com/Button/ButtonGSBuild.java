package com.Button;

import java.awt.event.ActionEvent;
import com.CommandBat.GaemServerBuildBat;

public class ButtonGSBuild extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonGSBuild() {
		this.setText("GSBuild");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new GaemServerBuildBat(textCmd.getTextA()));
	}

}
