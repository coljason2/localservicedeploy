package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.StartBat;

public class ButtonStart extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonStart() {
		this.setText("Start");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new StartBat(textCmd.getTextA()));
		cleanRedis();
	}

}
