package com.Button;

import java.awt.event.ActionEvent;

import com.CommandBat.ApiBuildBat;

public class ButtonAPIBuild extends GenerateButton {
	private static final long serialVersionUID = 1L;

	public ButtonAPIBuild() {
		this.setText("APIBuild");
	}

	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
		singleThreadExecutor.execute(new ApiBuildBat(textCmd.getTextA()));
	}

}
