package com.MenuItem;

import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;

import java.awt.Cursor;

/**
 * Link
 * 
 * this class creates a Link extending JLabel
 * 
 * @author Martin Scharm, see http://binfalse.de
 *
 */
public class LinkMenuItem extends javax.swing.JMenuItem implements java.awt.event.MouseListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String url;

	public LinkMenuItem() {
		super();
		init("");
	}

	public LinkMenuItem(javax.swing.Icon image) {
		super(image);
		init("");
	}

	public LinkMenuItem(String text) {
		super(text);
		init(text);
	}

	public JMenuItem setURL(String url) {
		this.url = url;
		this.setToolTipText("Open " + url + " in your browser");
		return this;
	}

	private void init(String url) {
		setURL(url);
		this.addMouseListener(this);
		// this.setForeground(Color.BLUE);
	}

	public LinkMenuItem(String text, int horizontalAlignment) {
		super(text, horizontalAlignment);
		init(text);
	}


	public void mouseClicked(MouseEvent arg0) {
		// browse();
	}


	public void mouseEntered(MouseEvent arg0) {
		setCursor(new Cursor(Cursor.HAND_CURSOR));
	}


	public void mouseExited(MouseEvent arg0) {
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}


	public void mousePressed(MouseEvent arg0) {
		System.out.println("-------------MouseEvent arg0-----------" + arg0.getID());
		browse();
	}


	public void mouseReleased(MouseEvent arg0) {
	}

	private void browse() {
		if (java.awt.Desktop.isDesktopSupported()) {
			java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
			if (desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
				try {
					desktop.browse(new java.net.URI(url));
					return;
				} catch (java.io.IOException e) {
					e.printStackTrace();
				} catch (java.net.URISyntaxException e) {
					e.printStackTrace();
				}
			}
		}

		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
			} else if (osName.startsWith("Mac OS")) {
				Class<?> fileMgr = Class.forName("com.apple.eio.FileManager");
				java.lang.reflect.Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[] { String.class });
				openURL.invoke(null, new Object[] { url });
			} else {
				// check for $BROWSER
				java.util.Map<String, String> env = System.getenv();
				if (env.get("BROWSER") != null) {
					Runtime.getRuntime().exec(env.get("BROWSER") + " " + url);
					return;
				}

				// check for common browsers
				String[] browsers = { "firefox", "iceweasel", "chrome", "opera", "konqueror", "epiphany", "mozilla",
						"netscape" };
				String browser = null;
				for (int count = 0; count < browsers.length && browser == null; count++)
					if (Runtime.getRuntime().exec(new String[] { "which", browsers[count] }).waitFor() == 0) {
						browser = browsers[count];
						break;
					}
				if (browser == null)
					throw new RuntimeException("couldn't find any browser...");
				else
					Runtime.getRuntime().exec(new String[] { browser, url });
			}
		} catch (Exception e) {
			javax.swing.JOptionPane.showMessageDialog(null,
					"couldn't find a webbrowser to use...\nPlease browser for yourself:\n" + url);
		}
	}
}
