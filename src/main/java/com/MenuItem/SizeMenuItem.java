package com.MenuItem;

import java.awt.Font;
import java.awt.event.MouseEvent;



public class SizeMenuItem extends GenerateMenultem {

	private static final long serialVersionUID = 1L;
	private int size;

	public SizeMenuItem(String string) {
		super();
		setText("Size " + string);
		size = Integer.parseInt(string);
	}


	public void mousePressed(MouseEvent e) {
		textCmd.textFont = new Font(textCmd.textFont.getName(), textCmd.textFont.getStyle(), size);
		textCmd.textA.setFont(textCmd.textFont);
		PropertiesUtil.setTextFont(textCmd.textFont);
		PropertiesUtil.setTextCmd(textCmd);
		PropertiesUtil.SaveConfig();
	}


}
