package com.MenuItem;

import java.awt.event.MouseEvent;

import com.MainFrame.PathSetting;

public class PathMenuItem extends GenerateMenultem  {
	PathSetting pathSetting = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PathMenuItem(String string) {
		super();
		setText(string);
	}

	public void mousePressed(MouseEvent e) {
		System.out.println("mousePressed");

		if (pathSetting == null) {
			PathSetting pathSetting = new PathSetting();
			System.err.println(pathSetting);
		} else {
			pathSetting.getFrmPathsetting().setVisible(true);
		}
	}

}
