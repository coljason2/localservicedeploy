package com.CommandBat;

import javax.swing.JTextArea;

public class ApiBuildBat extends GenerateCommand {

	private final static String Buildbat = current_path + "/conf/ApiBuild.bat";

	public ApiBuildBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Buildbat);
	}

}
