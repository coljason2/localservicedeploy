package com.CommandBat;

import javax.swing.JTextArea;

public class StartLobbyBat extends GenerateCommand {
	private final static String Startbat = current_path + "/conf/StartLobby.bat";

	public StartLobbyBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Startbat);
	}

}
