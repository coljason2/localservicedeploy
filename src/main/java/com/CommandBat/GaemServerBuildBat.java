package com.CommandBat;

import javax.swing.JTextArea;

public class GaemServerBuildBat extends GenerateCommand {

	private final static String Buildbat = current_path + "/conf/GameServerBuild.bat";

	public GaemServerBuildBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Buildbat);
	}

}
