package com.CommandBat;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import javax.swing.JTextArea;

public abstract class GenerateCommand implements Runnable {

	protected static JTextArea area;
	protected static final File now_directory = new File(".");
	protected static final String current_path = now_directory.getAbsolutePath().replaceAll("\\.", "");

	public static void run_command(String bat_path) {
		try {
			area.setText("");
			Process process = Runtime.getRuntime().exec(bat_path);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "Big5"));
			String s;
			while ((s = reader.readLine()) != null) {
				area.append(s + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
			area.append(e.getMessage() + "\n");
		}
	}

	public GenerateCommand(JTextArea area) {
		GenerateCommand.area = area;
	}

}
