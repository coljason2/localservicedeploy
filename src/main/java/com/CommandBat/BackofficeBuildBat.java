package com.CommandBat;

import javax.swing.JTextArea;

public class BackofficeBuildBat extends GenerateCommand {

	private final static String Buildbat = current_path + "/conf/BackofficeBuild.bat";

	public BackofficeBuildBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Buildbat);
	}

}
