package com.CommandBat;

import javax.swing.JTextArea;

public class BuildBat extends GenerateCommand {

	private final static String Buildbat = current_path + "/conf/Build.bat";

	public BuildBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Buildbat);
	}

}
