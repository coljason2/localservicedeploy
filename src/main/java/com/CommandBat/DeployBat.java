package com.CommandBat;

import javax.swing.JTextArea;

public class DeployBat extends GenerateCommand {

	private final static String Deploybat = current_path + "/conf/DeployToLocalDev.bat";

	public DeployBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Deploybat);
	}

}
