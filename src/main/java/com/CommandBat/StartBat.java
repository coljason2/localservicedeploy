package com.CommandBat;


import javax.swing.JTextArea;

public class StartBat extends GenerateCommand {

	private final static String Startbat = current_path + "/conf/Start.bat";

	public StartBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Startbat);
	}

}
