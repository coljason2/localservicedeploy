package com.CommandBat;


import javax.swing.JTextArea;

public class RestartBat extends GenerateCommand {

	private final static String Restartbat = current_path + "/conf/Restart.bat";

	public RestartBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Restartbat);
	}

}
