package com.CommandBat;

import javax.swing.JTextArea;

public class StopBat extends GenerateCommand {

	private final static String Stopbat = current_path + "/conf/Stop.bat";

	public StopBat(JTextArea area) {
		super(area);
	}

	public void run() {
		run_command(Stopbat);
	}

}
