@echo off

@set eclipse_path="C:\eclipse-workspace\LottoKeno"
@set local_dev_path="C:\Users\junchi\Desktop"

@set res_web_path=%eclipse_path%"\MerchantSite\web\dist"
@set res_app_path=%eclipse_path%"\MerchantSite\app\dist"
@set res_api_path=%eclipse_path%"\MerchantApiPortal\dist"
@set res_server_path=%eclipse_path%"\GameServer\app\dist\"
@set res_lobby_path=%eclipse_path%"\MemberSite\dist\"


@set kn_web_path=%local_dev_path%"\KN\kn-merchant-web\webapps\kn-merchant-web"
@set kn_app_path=%local_dev_path%"\KN\kn-merchant-web\webapps\kn-merchant-app"
@set kn_api_path=%local_dev_path%"\KN\kn-merchant-web\webapps\kn-api"
@set lt_web_path=%local_dev_path%"\LT\lt-merchant-web\webapps\lt-merchant-web"
@set lt_app_path=%local_dev_path%"\LT\lt-merchant-web\webapps\lt-merchant-app"
@set lt_api_path=%local_dev_path%"\LT\lt-merchant-web\webapps\lt-api-portal"
@set kn_server_path=%local_dev_path%"\KN\kn-game-server"
@set lt_server_path=%local_dev_path%"\LT\lt-game-server"
@set kn_lobby_path=%local_dev_path%"\KN\kn-web\webapps\kn-web\"
@set lt_lobby_path=%local_dev_path%"\LT\lt-web\webapps\lt-web\"


echo 清除舊有部屬檔案.......
cd %kn_web_path%
IF EXIST lk-merchant-site-web-keno_*.war (
del lk-merchant-site-web-keno_*.war)

cd %kn_app_path%
IF EXIST  lk-merchant-site-app-keno_*.war (
del lk-merchant-site-app-keno_*.war)

cd %kn_api_path%
IF EXIST lk-merchant-api-portal-keno_*.war (
del lk-merchant-api-portal-keno_*.war)

cd %lt_web_path%
IF EXIST lk-merchant-site-web-lotto_*.war (
del lk-merchant-site-web-lotto_*.war)

cd %lt_app_path%
IF EXIST lk-merchant-site-app-lotto_*.war (
del lk-merchant-site-app-lotto_*.war)

cd %lt_api_path%
IF EXIST lk-merchant-api-portal-lotto_*.war (
del lk-merchant-api-portal-lotto_*.war)

cd %kn_server_path%
IF EXIST lk-server-keno_*.tar.* (
del lk-server-keno_*.tar.*)

cd %lt_server_path%
IF EXIST lk-server-lotto_*.tar.* (
del lk-server-lotto_*.tar.*)

cd %kn_lobby_path%
IF EXIST lk-lobby-keno_*.war (
del lk-lobby-keno_*.war)

cd %lt_lobby_path%
IF EXIST lk-lobby-lotto_*.war (
del lk-lobby-lotto_*.war)


echo 清除完畢.................



echo 清除舊有lib檔案..........
cd %kn_web_path%\WEB-INF
del /Q lib
cd %kn_app_path%\WEB-INF
del /Q lib
cd %kn_api_path%\WEB-INF
del /Q lib
cd %kn_lobby_path%\WEB-INF
del /Q lib
cd %lt_web_path%\WEB-INF
del /Q lib
cd %lt_app_path%\WEB-INF
del /Q lib
cd %lt_api_path%\WEB-INF
del /Q lib
cd %lt_lobby_path%\WEB-INF
del /Q lib
cd %kn_server_path%
del /Q lib
cd %lt_server_path%
del /Q lib
echo 清除完畢.................



echo 搬移檔案.......
cd %res_web_path%
copy lk-merchant-site-web-keno_*.war   %kn_web_path%
copy lk-merchant-site-web-lotto_*.war  %lt_web_path%
cd %res_app_path%
copy lk-merchant-site-app-keno_*.war   %kn_app_path%
copy lk-merchant-site-app-lotto_*.war  %lt_app_path%
cd %res_api_path%
copy lk-merchant-api-portal-keno_*.war   %kn_api_path%
copy lk-merchant-api-portal-lotto_*.war  %lt_api_path%
cd %res_server_path%
copy lk-server-keno_*.tar.gz    %kn_server_path%
copy lk-server-lotto_*.tar.gz	%lt_server_path%
cd %res_lobby_path%
copy lk-lobby-keno_*.war        %kn_lobby_path%
copy lk-lobby-lotto_*.war       %lt_lobby_path%
echo 搬移完成.......


echo 佈署檔案中，請勿關閉.........................
cd %kn_web_path%
jar xf  lk-merchant-site-web-keno_*.war
cd %kn_app_path%
jar xf  lk-merchant-site-app-keno_*.war
cd %lt_web_path%
jar xf  lk-merchant-site-web-lotto_*.war
cd %lt_app_path%
jar xf  lk-merchant-site-app-lotto_*.war
cd %kn_api_path%
jar xf  lk-merchant-api-portal-keno_*.war
cd %lt_api_path%
jar xf  lk-merchant-api-portal-lotto_*.war
cd %kn_lobby_path%
jar xf  lk-lobby-keno_*.war
cd %lt_lobby_path%
jar xf  lk-lobby-lotto_*.war  
cd %kn_server_path%
7z x lk-server-keno_*.tar.gz -y
7z x lk-server-keno_*.tar -y
cd %lt_server_path%
7z x lk-server-lotto_*.tar.gz -y
7z x lk-server-lotto_*.tar -y
echo 佈署完成..........................

pause

