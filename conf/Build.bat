@echo off
@set eclipse_path="C:\SG-workspace\Spade"



echo build_center_server.............
cd %eclipse_path%
cd MerchantApi
call "ant"
cd ../common
call "ant"
cd ../GameServer/Api
call "ant"
cd ../CenterCore
call "ant"
cd ../CenterApp
call "ant"
cd ../../Accessory/App
call "ant"

echo build_game_server.............
cd %eclipse_path%
cd MerchantApi
call "ant"
cd ../common
call "ant"
cd ../iGaming/iGamingApi
call "ant"
cd ../../GameServer/Api
call "ant"
cd ../rng
call "ant"
cd ../Core
call "ant"
cd ../App
call "ant"
cd ../Mock
call "ant"

echo build_lobby.............
cd %eclipse_path%
cd MerchantApi
call "ant"
cd ../common
call "ant"
cd ../iGaming/iGamingApi
call "ant"
cd ../../Provider/Common
call "ant"
cd ../EvApi
call "ant"
cd ../AbApi
call "ant"
cd ../AgApi
call "ant"
cd  ../../Lobby/LobbyWeb
call "ant"
cd  ../LobbyApp
call "ant"

echo build_merchant_api.............
cd %eclipse_path%
cd MerchantApi
call "ant"
cd ../common
call "ant"
cd ../iGaming/iGamingApi
call "ant"
cd ../../MerchantApiPortal
call "ant"

echo build_merchant_site.............
cd %eclipse_path%
cd MerchantApi
call "ant"
cd ../common
call "ant"
cd ../iGaming/iGamingApi
call "ant"
cd  ../../MerchantSite/App
call "ant"
cd ../Web
call "ant"

pause
