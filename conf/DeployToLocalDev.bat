@echo off

@set eclipse_path="C:\SG-workspace\Spade"
@set local_dev_path="C:\Users\junchi\Desktop"

@set res_eclipse_path=%eclipse_path%"\dist"
@set egame_api=%local_dev_path%"\SG\sg-merchant-web\webapps\egame_api"
@set egame_lobby_app=%local_dev_path%"\SG\sg-merchant-web\webapps\egame_lobby_app"
@set egame_merchant_app=%local_dev_path%"\SG\sg-merchant-web\webapps\egame_merchant_app"
@set egame_merchant_web=%local_dev_path%"\SG\sg-merchant-web\webapps\egame_merchant_web"
@set egame_lobby_public=%local_dev_path%"\SG\sg-web\webapps\egame_lobby_public"
@set sg-game-server=%local_dev_path%"\SG\sg-game-server\dist"



echo ?M????????????.......
cd %egame_api%
IF EXIST spade-merchant-api-portal_*.war (
del spade-merchant-api-portal_*.war)

cd %egame_lobby_app%
IF EXIST  spade-lobby-app_*.war (
del spade-lobby-app_*.war)

cd %egame_merchant_app%
IF EXIST spade-merchant-app_*.war (
del spade-merchant-app_*.war)

cd %egame_merchant_web%
IF EXIST spade-merchant-web_*.war (
del spade-merchant-web_*.war)

cd %egame_lobby_public%
IF EXIST spade-lobby-web_*.war (
del spade-lobby-web_*.war)

echo ?M??????.................






echo ?h?????.......
cd %res_eclipse_path%
copy spade-merchant-api-portal_*.war   %egame_api%
cd %res_eclipse_path%
copy spade-lobby-app_*.war      %egame_lobby_app%
cd %res_eclipse_path%
copy spade-merchant-app_*.war   %egame_merchant_app%
cd %res_eclipse_path%
copy spade-merchant-web_*.war   %egame_merchant_web%
cd %res_eclipse_path%
copy spade-lobby-web_*.war       %egame_lobby_public%
echo ?h??????.......


echo ?G?p?????A???????.........................

cd %egame_api%
jar xf  spade-merchant-api-portal_*.war
cd %egame_lobby_app%
jar xf  spade-lobby-app_*.war
cd %egame_merchant_app%
jar xf  spade-merchant-app_*.war
cd %egame_merchant_web%
jar xf  spade-merchant-web_*.war
cd %egame_lobby_public%
jar xf  spade-lobby-web_*.war 
cd %res_eclipse_path%
copy igaming-wallet-api.jar        %sg-game-server%
copy spade-accessory-app.jar       %sg-game-server%
copy spade-common.jar              %sg-game-server%
copy spade-merchant-api.jar        %sg-game-server%
copy spade-server-api.jar          %sg-game-server%
copy spade-server-app.jar          %sg-game-server%
copy spade-server-center-core.jar  %sg-game-server%
copy spade-server-core.jar         %sg-game-server%
copy spade-server-rng.jar          %sg-game-server%

echo ?G?p????..........................

pause

