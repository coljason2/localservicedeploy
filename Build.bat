@echo off
@set eclipse_path="C:\eclipse-workspace\LottoKeno"

cd %eclipse_path%
cd common
call "ant"
cd ../MerchantApi
call "ant"
cd ../GameServerApi
call "ant"
cd ../GameServer/core
call "ant"
cd ../app
call "ant"
cd ../../AwardNumber
call "ant"
rem cd ../SyncServer
rem call "ant"
rem cd ../SyncClient
rem call "ant"
cd ../..

cd %eclipse_path%
cd common
call "ant"
cd ../MerchantApi
call "ant"
cd ../MerchantApiPortal
call "ant"
cd ../MemberSite
call "ant"

cd %eclipse_path%
cd common
call "ant"
cd  ../MerchantSite/app
call "ant"
cd ../web
call "ant"
cd ../..

pause
